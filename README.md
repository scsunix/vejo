# Cluster Resource Monitor                                                                                             

![demo image](doc/img/default-design.png)

### Dependency
* python packages: Jinja2, Flask, psutil, nvidia-ml-py (Python2) or nvidia-ml-py3 (Python3)
* **responsive** web design implemented with plain CSS and JavaScript, and is therefore lightweight and can be deployed without internet access
* tested with both Python 2 and 3

### Trouble shooting
Find the error log here: /tmp/monitor-<node name>.log


